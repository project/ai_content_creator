<?php

declare(strict_types=1);
namespace Drupal\ai_content_creator\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Config\ConfigFactoryInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Function to create the AIContentCreator Config Form.
 */
class AiContentCreatorConfigForm extends ConfigFormBase {

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * Constructs for AiContentCreatorConfigForm object.
   *
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The factory for configuration objects.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The EntityTypeManager.
   */
  public function __construct(ConfigFactoryInterface $config_factory, EntityTypeManagerInterface $entity_type_manager) {
    parent::__construct($config_factory);
    $this->entityTypeManager = $entity_type_manager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('config.factory'),
      $container->get('entity_type.manager')
    );
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      'ai_content_creator.adminsettings',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'ai_content_creator_settings_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config('ai_content_creator.adminsettings');
    // $node_types = NodeType::loadMultiple();
    $node_storage = $this->entityTypeManager->getStorage('node_type');
    $node_types = $node_storage->loadMultiple();
    $options = [];
    $form['api_url'] = [
      '#type' => 'textfield',
      '#title' => $this->t('OpenAI URL endpoint'),
      '#description' => $this->t('Please provide the OpenAI API URL key.'),
      '#default_value' => $config->get('api_url') ?? 'https://api.openai.com/v1/completions',
      '#required' => TRUE,
    ];
    $form['api_key'] = [
      '#type' => 'textfield',
      '#title' => $this->t('OpenAI access token'),
      '#description' => $this->t('Please provide the OpenAI API access token key.'),
      '#default_value' => $config->get('api_key'),
      '#required' => TRUE,
    ];
    $form['api_max_token'] = [
      '#type' => 'textfield',
      '#title' => $this->t('OpenAI max token'),
      '#description' => $this->t('The maximum number of tokens to generate in the completion. "1 token = 4 characters in English"'),
      '#default_value' => $config->get('api_max_token') ?? 2000,
      '#required' => TRUE,
    ];
    foreach ($node_types as $node_type) {
      $options[$node_type->id()] = $node_type->label();
    }
    $form['api_node_type'] = [
      '#type' => 'checkboxes',
      '#title' => $this->t('Select the content types'),
      '#options' => $options,
      '#default_value' => $config->get('api_node_type') ?? [],
    ];
    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    parent::submitForm($form, $form_state);

    $this->config('ai_content_creator.adminsettings')
      ->set('api_url', $form_state->getValue('api_url'))
      ->set('api_key', $form_state->getValue('api_key'))
      ->set('api_node_type', $form_state->getValue('api_node_type'))
      ->set('api_max_token', $form_state->getValue('api_max_token'))
      ->save();
  }

}
