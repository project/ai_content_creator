# AI Content Creator

The AI Content Creator module is a powerful tool for generating high-quality
content for your Drupal website. It uses advanced natural language processing
algorithms to write articles, blog posts, and other types of content in seconds.
With its intuitive interface, you can easily specify the topic and tone of 
the content you want to create, and the AI will do the rest. Whether you need 
to generate content for a new blog post, update your website's product
descriptions, or create engaging social media posts, the AI Content Creator
is the perfect solution. Save time and effort while ensuring that your
website always has fresh, engaging content.

### AI Content generator node form

![AI Content generator node form](https://i.imgur.com/EUHS0tz.png)

### AI Content generator setting form

![AI Content generator setting form](https://i.imgur.com/z9WD5Yz.png)


## Configuration

1. Navigate to Administration > Configuration >
   AI Content Creator API Settings (/admin/config/ai_content_creator)
   to configure OpenAI API settings
2. Enter your API Access token and what content types the feature will appear.
3. New section "AI Content Generator" will appear in content
   types enabled in the settings

## Maintainers && credit

- Abderrahim GHAZALI - [AbdeI](https://www.drupal.org/u/abdei)